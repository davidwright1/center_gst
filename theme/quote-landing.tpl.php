<?php
/**
 * @file
 * Template file of post-design Quote landing page.
 */
?>

<div class="quote-landing">
  <h1 class="quote-landing-header"><?php print $title; ?></h1>

  <div class="quote-landing-info">
    <div class="quote-landing-img-wrapper span6"><img class="quote-landing-img" src="<?php print $img_preview; ?>"></div>
    <div class="quote-landing-text span6">
      <h3>Title: <?php print $quote_title; ?></h3>
      <h3>Description: <?php print $product_title; ?></h3>
      <h3 class="quotes-price">Estimate: <span class="quote-landing-price"><?php print $quote_price; ?></span></h3>
    </div>
  </div>

  <div class="quote-landing-decision span6">
    <h2 class="quote-landing-prompt">Now, what would you like to do?</h2>
    <div class="quote-landing-options">
      <div class="quote-buy-form"><?php print $buy_now_button; ?></div>
      <a class="btn btn-default quote-landing-btn" href="<?php print $gst_url; ?>">I Want to Make Another Quote</a>
      <div class="quote-email-form"><?php print $quote_email_button; ?></div>
    </div>
  </div>
</div>
