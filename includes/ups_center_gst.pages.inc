<?php
/**
 * Created by PhpStorm.
 * User: dwright
 * Date: 2019-06-05
 * Time: 11:35
 */

/**
 * Quote Page for end users to select various CTAs.
 */
function ups_center_gst_quote_page() {
  $get = drupal_get_query_parameters();
  if (isset($get['uuid'])) {
    if (is_string($get['uuid'])) {
      $quote = ups_center_gst_get_quote($get['uuid']);
      if ($quote) {
        drupal_add_css(drupal_get_path('module', 'ups_center_gst') . '/theme/ups_center_gst_quote_landing.css');
        return theme('quote_landing', ['quote' => $quote, 'mode' => 'anonymous']);
      }
    }
  }
  drupal_goto('/');
}

/**
 * User Quotes page under My Account.
 * Returns list of quotes associated with current user.
 */
function ups_center_gst_user_quote_page($user) {

  if (is_numeric($user)) {
    $user = user_load($user);
  }
  $mail = $user->mail;

  $config = ups_center_gst_determine_environment();

  $url = $config['bo_url'] . '/user/' . $mail . '/quotes';

  $quotes = [];
  try {
    $request = drupal_http_request($url);
//    $connection = clients_connection_load('backoffice_connection');
//    $orders = $connection->makeRequest('backoffice_customer_order_list/' . $mail . '/' . $uuid, 'GET', []);
  } catch (Exception $e) {
    watchdog("UPS Center core Error", $e->getMessage());
  }

  if (empty($request->data)) {
    return t('You currently have no quotes.');
  }
  else {
    $quotes = drupal_json_decode($request->data);
  }

  $output = '';

  $header = [
    'Title',
    'Status',
    'Created',
    'Estimate',
    'Operations' // View Details, Send Me Email, Checkout/Buy.
  ];
  $rows = [];

  // Fashion some html output for the request data.
  foreach ($quotes as $quote) {
    $data = [];
    $data[] = [
      'data' => $quote['title'] . ': #' . $quote['qid'],
      'data-label' => 'Title'
    ];
    $data[] = [
      'data' => $quote['status'],
      'data-label' => 'Status'
    ];
    $data[] = [
      'data' => format_date($quote['created'], 'medium'),
      'data-label' => 'Created',
    ];
    $data[] = [
      'data' => commerce_currency_format($quote['field_quote_price']['und'][0]['amount'], 'USD'),
      'data-label' => 'Estimate'
    ];

    //
    // Operations column.
    $view_url = '/user/' . $user->uid . '/quote/' . $quote['qid'];

    $operations = '<div>';
    // View Details
    $operations .= "<a href='$view_url' class='btn btn-primary'>Details</a>";
    // Send Me Email
    $vars = [
      'user' => $user,
      'quote' => $quote
    ];
    $email_form = drupal_get_form('ups_center_gst_quotes_email_form', $vars);
    $operations .= drupal_render($email_form);
    // Buy this Quote
    $buy_form = drupal_get_form('ups_center_gst_quotes_buy_form', $vars);
    $operations .= drupal_render($buy_form);
    $operations .= '</div>';

    $data[] = [
      'data' => $operations,
      'data-label' => 'Operations'
    ];

    $rows[] = $data;
  }

  $output .= theme('table',
    array(
      'header' => $header,
      'rows' => $rows,
      'attributes' => array('class' => array('labeled-responsive-table customer-order-list table table-bordered'))
    ));

  return $output;
}

/**
 * Single Quote page with more details.
 */
function ups_center_gst_user_quote_detail($user, $qid) {

  $quote = ups_center_gst_get_quote($qid);
  if (is_string($quote)) {
    $quote = drupal_json_decode($quote);
  }

  if ($quote) {
    drupal_set_title('Quote #' . $quote['qid']);
    $this_center = variable_get("ups_center_id", NULL);

//    if (strtolower($user->mail) != strtolower($order->mail)) {
//      drupal_access_denied();
//    }

    $output = '';
    $output .= '<h2 class="order-page-title">Quote #' . $qid . ' - Quote Created
     ' . format_date($quote['created'], 'medium') . '</h2>';

    //  Top Area
    $output .= '<div class="contact-wrapper well clearfix">';

    $description = $quote['product_size'] . ' ' .
      str_replace(' Quote', '', $quote['title']) . ' on ' . $quote['stock_option']
      . ' ' . $quote['coating_option'] . ' ' . $quote['folding_option'];

    $job = ps_core_get_job($quote['project_pscore_id']);
    if (!$job && !empty($quote['field_quote_project'])) {
      $project = printsites_project_load($quote['field_quote_project']['und'][0]['target_id']);
    }

    $url = $job->project->production_asset->preview->uri;
    $preview = "<img class='' src='$url'>";

    $header = [
      'Preview',
      'Title',
      'Description',
      'Status',
      'Quantity',
      'Estimate',
    ];

    $data[] = [
      'data' => $preview,
      'data-label' => 'Preview',
    ];
    $data[] = [
      'data' => $quote['title'],
      'data-label' => 'Title'
    ];
    $data[] = [
      'data' => $description,
      'data-label' => 'Description'
    ];
    $data[] = [
      'data' => $quote['status'],
      'data-label' => 'Status'
    ];
    $data[] = [
      'data' => $quote['runsize_number'],
      'data-label' => 'Quantity'
    ];
    $data[] = [
      'data' => commerce_currency_format($quote['field_quote_price']['und'][0]['amount'], 'USD'),
      'data-label' => 'Estimate'
    ];


    $rows[] = $data;
    $output .= theme('table',
      array(
        'header' => $header,
        'rows' => $rows,
        'attributes' => array('class' => array('labeled-responsive-table customer-order-list table table-bordered'))
      ));

    return $output;
  }
  else {
    return 'heto';
  }
}
