<?php
/**
 * @file: Contains service resources and menu callbacks for Center-side GST functionality.
 */

include_once 'ups_center_gst.inc';

/**
 * GST - product select.
 * @return array: List of enabled product displays for GST.
 */
function ups_center_gst_start() {
  ups_center_gst_cors_headers();

  $gst_enabled_displays = [
    'Business Cards',
    'Menus',
    'Brochures',
    'Banners',
    'Bookmarks',
    'Car Magnets',
    'Door Hangers',
    'Envelopes',
    'Indoor Magnets',
    'Flyers',
    'Folders',
    'Lawn Signs',
    'Letterhead',
    'Notepads',
    'Personalized Calendars',
    'Presentation Folders',
    'Postcards',
    'Rack Cards',
  ];

  // Fetch initial view and return JSON.
  $types = ['flat', 'adhoc'];
  $array = [];
  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'product_display')
    ->propertyCondition('title', $gst_enabled_displays, 'IN')
    ->fieldCondition('field_display_category', 'value', $types, 'IN')
    ->propertyOrderBy('title', 'asc');
  $results = $query->execute();

  if (isset($results['node'])) {
    foreach ($results['node'] as $result) {
      // Need title, thumbnail, and nid.
      $node = node_load($result->nid);
      $array[] = [
        'id' => $node->nid,
        'type' => strtolower(str_replace(' ', '_', $node->title)),
        'title' => $node->title,
        'thumbnail' => $node->field_product_thumbnail['und'][0]['uri']
      ];
    }
  }

  watchdog('GST: Product List', drupal_json_encode($array));


  return $array;
}

/**
 * Start quote with node data.
 */
function ups_center_gst_start_quote($data, $nid) {
  ups_center_gst_cors_headers();
  if (is_numeric($nid)) {
    $node = node_load($nid);
    $array = [
      "id" => $nid,
      "type" => strtolower(str_replace(' ', '_', $node->title)),
      "title" => $node->title,
      "thumbnail" => $node->field_product_thumbnail['und'][0]['uri']
    ];

    switch ($array['type']) {
      case '';
        break;
    }
    return $array;
  }
  else {
    return FALSE;
  }
}

/**
 * Quote attribute options.
 */
function ups_center_gst_quote_sizes($quote) {
  ups_center_gst_cors_headers();
  watchdog('DAVID', drupal_json_encode($quote));

  $collection = $quote['field_flat_product_collection']['und'][0]['value'];
  if ($collection == 'rack_cards') {
    $collection = 'rack_card';
  }
  elseif ($collection == 'car_magnets') {
    $collection = 'car_magnet';
  }
  elseif ($collection == 'letterhead') {
    $collection = 'letterheads';
  }
  elseif ($collection == 'presentation_folders') {
    $collection = 'folders';
  }

  $terms = [];
  $size_terms = [];

  $select = db_query("
    SELECT DISTINCT frn.field_runsize_number_tid 
    FROM {field_data_field_runsize_number} frn 
    INNER JOIN {field_data_field_flat_product_collection} fpc 
    ON fpc.entity_id=frn.entity_id 
    INNER JOIN {commerce_product} cp 
    ON frn.entity_id=cp.product_id
    WHERE fpc.field_flat_product_collection_value=:collection 
    AND cp.status = 1
  ", [':collection' => $collection])->fetchAll();

  foreach ($select as $result) {
    $term = taxonomy_term_load($result->field_runsize_number_tid);
    $terms[] = $term->name;
  }

  $query = db_query("
    SELECT DISTINCT frn.field_product_size_tid 
    FROM {field_data_field_product_size} frn 
    INNER JOIN {field_data_field_flat_product_collection} fpc 
    ON fpc.entity_id=frn.entity_id 
    INNER JOIN {commerce_product} cp 
    ON frn.entity_id=cp.product_id
    WHERE fpc.field_flat_product_collection_value=:collection 
    AND cp.status = 1
  ",[':collection' => $collection])->fetchAll();

  foreach ($query as $item) {
    $size_term = taxonomy_term_load($item->field_product_size_tid);
    $size_terms[] = $size_term->name;
  }

  $min_price_query = db_query("
    SELECT min(fcp.commerce_price_amount)
    FROM field_data_commerce_price fcp 
    INNER JOIN commerce_product cp 
    ON fcp.entity_id = cp.product_id 
    INNER JOIN field_data_field_flat_product_collection ffpc 
    ON fcp.entity_id = ffpc.entity_id
    WHERE ffpc.field_flat_product_collection_value=:collection
    AND cp.status = 1
  ", [':collection' => $collection])->fetchField();

  $min_price_query = $min_price_query ?
    commerce_currency_format($min_price_query, 'USD') : NULL;

  $return = [
    'runsizes' => $terms,
    'productsizes' => $size_terms,
    'minprice' => $min_price_query
  ];

  return $return;
}

/**
 * Quote validation - makes sure a SKU is selected.
 */
function ups_center_gst_validate($data) {
  ups_center_gst_cors_headers();

  watchdog('VALIDATE', drupal_json_encode($data));

  if ($data != null && !empty($data)) {

    $quote = is_array($data) ? $data['quote'] : $data->quote;
    $quote = (object) $quote;
    $collection = $quote->field_flat_product_collection['und'][0]['value'];

    $field_list = ups_center_gst_field_list();

    // Figure out which tids we're using.
    foreach ($field_list as $field_name => $type) {
      if ($type == 'tid') {
        $property = str_replace('field_', '', $field_name);

        //
        // Naturally, TUPSS has non-standardized vocabulary names.
        $vocab = $property;
        if ($property == 'grommet_option') {
          $vocab = 'product_option_grommets';
        }
        else if ($property == 'hem_option') {
          $vocab = 'product_option_hems';
        }
        else if ($property == 'pole_pocket_option') {
          $vocab = 'product_option_pole_pockets';
        }

        if ($quote->$property != '' && !is_null($quote->$property)) {
          $terms = taxonomy_get_term_by_name($quote->$property, $vocab);
          if (!empty($terms)) {
            $term = array_shift($terms);
            $tids[$field_name] = $term->tid;
          }
        }
      }
    }

    if (!in_array($collection, _ups_center_gst_folded_types())) {
      if (isset($tids['field_folding_option'])) {
        unset($tids['field_folding_option']);
      }
    }

    $query = new EntityFieldQuery();
    $query
      ->entityCondition('entity_type', 'commerce_product')
      ->propertyCondition('status', 1);

    foreach ($tids as $field => $tid) {
      $query->fieldCondition($field, 'tid', $tid, '=');
    }
    $results = $query->execute();

    watchdog('VALIDATION RESULTS', json_encode($results));

    if (isset($results['commerce_product']) && count($results['commerce_product']) == 1) {
      $product_id = array_shift($results['commerce_product']);
    }
    elseif (count($results['commerce_product']) == 2 ) {
      $product_id = array_shift($results['commerce_product']);
    }
    else {
      return 'Invalid Product';
    }
    $product = commerce_product_load($product_id->product_id);
    $return = [
      'sku' => $product->sku,
      'price' => $product->commerce_price['und'][0]['amount']
    ];
    return $return;
  }
  else {
    return "Validation API call failed. Please contact support.";
  }
}

/**
 * Refreshes attributes on GST.
 */
function ups_center_gst_refresh_attributes($data) {
  ups_center_gst_cors_headers();

  if ($data != NULL) {

    watchdog('Attribute Data', drupal_json_encode($data));

    $matching_products = ups_center_gst_matching_products($data);

    $field_list = ups_center_gst_field_list();
    $quote = is_array($data) ? $data['quote'] : $data->quote;
    $quote = (object)$quote;
    $display = node_load($quote->display_id);
    $display_wrapper = entity_metadata_wrapper('node', $display);
    $products = $display_wrapper->field_products->value();

    $matching_product = array_shift($matching_products);
    $matching_product_wrapper = entity_metadata_wrapper('commerce_product', $matching_product);

    // Type of tid means a "qualifying field," ie: an attribute field.
    foreach ($field_list as $field_name => $type) {
      if ($type == 'tid') {
        $field = field_info_field($field_name);
        $instance = field_info_instance('commerce_product', $field_name, 'flat');
        $properties = _options_properties('select', FALSE, TRUE, TRUE);
        $allowed_values = _options_get_options($field, $instance, $properties, 'commerce_product', $matching_product);

        if (!empty($allowed_values)) {
          $qualifying_fields[$field_name] = [
            'field' => $field,
            'instance' => $instance,
            'options' => $allowed_values
          ];
        }
      }
    }

    // Otherwise for products of varying types, display a simple select list
    // by product title.

    // TODO: this shouldn't happen, but maybe investigate further?

    if (!empty($qualifying_fields)) {
      $used_options = array();
      $field_has_options = array();

      foreach ($qualifying_fields as $field_name => $data) {
        // Build an options sarray of widget options used by referenced products.
        foreach ($products as $product_id => $product) {
          $product_wrapper = entity_metadata_wrapper('commerce_product', $product);

          if ($product->status == 1) {

            // Only add options to the present array that appear on products that
            // match the default value of the previously added attribute widgets.
            foreach ($used_options as $used_field_name => $unused) {
              // Don't apply this check for the current field being evaluated.
              if ($used_field_name == $field_name) {
                continue;
              }

              if (isset($attributes[$used_field_name]['default_value'])) {
                if ($product_wrapper->{$used_field_name}->raw() != $attributes[$used_field_name]['default_value']) {
                  continue 2;
                }
              }
            }

            // With our hard dependency on widgets provided by the Options
            // module, we can make assumptions about where the data is stored.
            if ($product_wrapper->{$field_name}->raw() != NULL) {
              $field_has_options[$field_name] = TRUE;
            }
            $used_options[$field_name][] = $product_wrapper->{$field_name}->raw();
          }
        }

        // If for some reason no options for this field are used, remove it
        // from the qualifying fields array.
        if (empty($field_has_options[$field_name]) || empty($used_options[$field_name])) {
          unset($qualifying_fields[$field_name]);
        } else {
          $attributes[$field_name] = [
            'options' => array_values(array_intersect_key($data['options'], drupal_map_assoc($used_options[$field_name]))),
            'default_ang_value' => taxonomy_term_load($matching_product_wrapper->{$field_name}->raw())->name,
            'default_value' => $matching_product_wrapper->{$field_name}->raw()
          ];
        }
      }
    }

    $price = commerce_currency_format($matching_product_wrapper->commerce_price->amount->value(), 'USD');
    $return = [
      'attributes' => $attributes,
      'price' => $price
    ];

    return $return;
  }
}

/**
 * Getting templates.
 */
function ups_center_gst_templates($data, $qid) {
  ups_center_gst_cors_headers();

  watchdog("DAVE",  json_encode($data));


//  $data = drupal_json_decode($data);
  $quote = (object) $data;

  if (isset($quote->sku)) {
    $product = commerce_product_load_by_sku($quote->sku);
    $uid = 0;

    //Create a new project.
    $data = [
      'title' => $quote->title . " Project",
      'type' => 'design',
      'project_generation_strategy' => 'direct_from_structure',
    ];

    if (empty($quote->field_quote_project)) {
      $project = printsites_project_new($data);
      $project->field_project_product['und'][0]['target_id'] = $product->product_id;
      $project->field_project_product_display['und'][0]['target_id'] = $quote->display_id;
      $project->field_project_state['und'][0]['value'] = "New";
      $project->field_quote_id['und'][0]['value'] = $quote->qid;
      $project->uid = $uid;

      printsites_project_save($project);
    }
    else {
      $project = printsites_project_load($quote->field_quote_project['und'][0]['target_id']);
    }

    $project = (object) $project;

    // Save Project ID to BO quote.
    $quote->field_quote_project = $project->id;
    $quote->project_pscore_id = $project->pscore_project_id;

    watchdog('GST: Quote object save to BO', drupal_json_encode($quote));
    ups_center_gst_save_quote($quote);

    $query = [];
    $query['parent_product'] = $product->product_id;

    $display = node_load($quote->display_id);
    if (!module_exists('ups_center_designer') || !ups_center_designer_enabled() || !ups_center_designer_is_display_supported($display)) {
      $product_query = ups_center_commerce_build_template_studio_query($product);
      $query = array_merge($query, $product_query);

      $product_query = ups_center_commerce_build_template_studio_query($product);
      $query = array_merge($query, $product_query);
      $go = 'designs/' . $display->nid . '/' . $product->product_id;
      drupal_goto($go, ["query" => $query]);
    }

    //drupal_add_http_header('Access-Control-Allow-Origin', '*');

    module_load_include('inc', 'search_api_page', 'search_api_page.pages');
    $view = search_api_page_view('template_browser');

    watchdog('template VIEW', json_encode($view));

    // Only return what Angular can use.
    if (isset($view['results'])) {
      foreach ($view['results']['#results']['results'] as $key => $result) {

          if (isset($result['fields']["field_surfaces:path"]) && isset($result['fields']["field_surfaces:position"])) {
            $paths = $result['fields']["field_surfaces:path"];
            $positions = $result['fields']["field_surfaces:position"];
          }

          $parameters = [
            'project_id' => $project->id,
            'nid' => $result['fields']['title'][0],
            'parent_product' => $product->product_id,
            'sku' => $quote->sku,
            'paths' => $paths,
            'position' => $positions
          ];
          $query = drupal_http_build_query($parameters);
          $return[] = $parameters + [
              'query' => $query,
              'image' => $result['fields']['field_front:file:url']
            ];

      }
    }
    else {
      $return = 'There was a problem querying for templates, please contact support.';
    }
  }
  else {
    $return = 'There was a problem querying for templates, please contact support.';
  }
  return $return;

}

/**
 * Returns an uploader URL.
 */
function ups_center_gst_upload($data) {
  ups_center_gst_cors_headers();

  $quote = (object) $data;

  // Can't use the product_id because BO and Centers have different product_ids.
  $product = commerce_product_load_by_sku($quote->sku);

  $query = [];
  $query['parent_product'] = $product->product_id;
  $query['quantity'] = $quote->runsize_number;
  $query['qid'] = $quote->qid;

  $product_query = ups_center_commerce_build_template_studio_query($product);
  $query = array_merge($query, $product_query);
  $querified = drupal_http_build_query($query);

  $href = '/studio/uploader?' . $querified;

  return $href;
}

/**
 * Banners options.
 *
 * Returns only options available for banners from previous attribute selections.
 */
function ups_center_gst_banners_options($data) {
  $quote = (object) $data;

  $select = db_query("
    SELECT DISTINCT frn.field_stock_option_tid 
    FROM {field_data_field_stock_option} frn 
    INNER JOIN {field_data_field_flat_product_collection} fpc 
    ON fpc.entity_id=frn.entity_id 
    INNER JOIN {commerce_product} cp 
    ON frn.entity_id=cp.product_id
    WHERE fpc.field_flat_product_collection_value='banners'
    AND cp.status = 1
  ")->fetchAll();


  foreach ($select as $res) {
    $term = taxonomy_term_load($res->field_stock_option_tid);
    $terms[] = $term->name;
  }

  $query = db_query("
    SELECT DISTINCT frn.field_product_size_tid 
    FROM {field_data_field_product_size} frn 
    INNER JOIN {field_data_field_flat_product_collection} fpc 
    ON fpc.entity_id=frn.entity_id 
    INNER JOIN {commerce_product} cp 
    ON frn.entity_id=cp.product_id
    WHERE fpc.field_flat_product_collection_value='banners' 
    AND cp.status = 1
  ")->fetchAll();

  foreach ($query as $item) {
    $size_term = taxonomy_term_load($item->field_product_size_tid);
    $size_terms[] = $size_term->name;
  }

  $query = db_query("
    SELECT DISTINCT frn.field_runsize_number_tid 
    FROM {field_data_field_runsize_number} frn 
    INNER JOIN {field_data_field_flat_product_collection} fpc 
    ON fpc.entity_id=frn.entity_id 
    INNER JOIN {commerce_product} cp 
    ON frn.entity_id=cp.product_id
    WHERE fpc.field_flat_product_collection_value='banners' 
    AND cp.status = 1
  ")->fetchAll();

  foreach ($query as $item) {
    $number = taxonomy_term_load($item->field_runsize_number_tid);
    $number_terms[] = $number->name;
  }

  $return = [
    'stocks' => $terms,
    'sizes' => $size_terms,
    'runsizes'=> $number_terms
  ];

  return $return;
}

/**
 * Gets a very limited list of products.
 */
function ups_center_gst_banners_skus($data) {
  $quote = (object) $data;

  //
  // 1. Save chosen options into BO.
  $ret = ups_center_gst_save_quote($quote);
  $quote = json_decode($ret);

  $sizes = taxonomy_get_term_by_name($quote->product_size, 'product_size');
  $size = array_shift($sizes);
  $size_tid = $size->tid;

  $stocks = taxonomy_get_term_by_name($quote->stock_option, 'stock_option');
  $stock = array_shift($stocks);
  $stock_tid = $stock->tid;

  $runsizes = taxonomy_get_term_by_name($quote->runsize_number, 'runsize_number');
  $runsize = array_shift($runsizes);
  $runsize_tid = $runsize->tid;

  //
  // 2. Find list of remaining products.  EFQ?
  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', 'commerce_product')
    ->entityCondition('bundle', 'flat')
    ->propertyCondition('status', 1, '=')
    ->fieldCondition('field_flat_product_collection', 'value', 'banners', '=')
    ->fieldCondition('field_product_size', 'tid', $size_tid, '=')
    ->fieldCondition('field_stock_option', 'tid', $stock_tid, '=')
    ->fieldCondition('field_runsize_number', 'tid', $runsize_tid, '=');
  $results = $query->execute();

  //
  // 3. Return limited list of pole pocket and grommet options.
  foreach($results['commerce_product'] as $result) {
    $product = commerce_product_load($result->product_id);
    $groms[] = $product->field_grommet_option['und'][0]['tid'];
    $poles[] = $product->field_pole_pocket_option['und'][0]['tid'];
    $hems[] = $product->field_hem_option['und'][0]['tid'];
  }

  $grom_tids = array_unique($groms);
  $pole_tids = array_unique($poles);
  $hem_tids = array_unique($hems);

  foreach ($grom_tids as $grom_tid) {
    $term = taxonomy_term_load($grom_tid);
    $grom_terms[] = $term->name;
  }
  foreach ($pole_tids as $pole_tid) {
    $pole_terms[] = taxonomy_term_load($pole_tid)->name;
  }
  foreach ($hem_tids as $hem_tid) {
    $hem_terms[] = taxonomy_term_load($hem_tid)->name;
  }

  $return = [
    'grommet_options' => $grom_terms,
    'pole_pocket_options' => $pole_terms,
    'hem_options' => $hem_terms
  ];

  return $return;
}

/**
 * Basically Refresh Attributes but with super limited Banners data.
 */
function ups_center_gst_banners_attributes($data) {
  $field_list = ups_center_gst_field_list();
  $matching_products = ups_center_gst_matching_products($data);
  $matching_product = array_shift($matching_products);
  $matching_product_wrapper = entity_metadata_wrapper('commerce_product', $matching_product);

  $display = ups_core_load_node_by_title('Banners', 'product_display');
  $display_wrapper = entity_metadata_wrapper('node', $display);
  $products = $display_wrapper->field_products->value();


  foreach ($field_list as $field_name => $type) {
    if ($type == 'tid') {
      $field = field_info_field($field_name);
      $instance = field_info_instance('commerce_product', $field_name, 'flat');
      $properties = _options_properties('select', FALSE, TRUE, TRUE);
      $allowed_values = _options_get_options($field, $instance, $properties, 'commerce_product', $matching_product);

      if (!empty($allowed_values)) {
        $qualifying_fields[$field_name] = [
          'field' => $field,
          'instance' => $instance,
          'options' => $allowed_values
        ];
      }
    }
  }

  if (!empty($qualifying_fields)) {
    $used_options = array();
    $field_has_options = array();

    foreach ($qualifying_fields as $field_name => $data) {
      // Build an options sarray of widget options used by referenced products.
      foreach ($products as $product_id => $product) {
        $product_wrapper = entity_metadata_wrapper('commerce_product', $product);

        if ($product->status == 1) {

          // Only add options to the present array that appear on products that
          // match the default value of the previously added attribute widgets.
          foreach ($used_options as $used_field_name => $unused) {
            // Don't apply this check for the current field being evaluated.
            if ($used_field_name == $field_name) {
              continue;
            }

            if (isset($attributes[$used_field_name]['default_value'])) {
              if ($product_wrapper->{$used_field_name}->raw() != $attributes[$used_field_name]['default_value']) {
                continue 2;
              }
            }
          }

          // With our hard dependency on widgets provided by the Options
          // module, we can make assumptions about where the data is stored.
          if ($product_wrapper->{$field_name}->raw() != NULL) {
            $field_has_options[$field_name] = TRUE;
          }
          $used_options[$field_name][] = $product_wrapper->{$field_name}->raw();
        }
      }

      // If for some reason no options for this field are used, remove it
      // from the qualifying fields array.
      if (empty($field_has_options[$field_name]) || empty($used_options[$field_name])) {
        unset($qualifying_fields[$field_name]);
      } else {
        $attributes[$field_name] = [
          'options' => array_values(array_intersect_key($data['options'], drupal_map_assoc($used_options[$field_name]))),
          'default_ang_value' => taxonomy_term_load($matching_product_wrapper->{$field_name}->raw())->name,
          'default_value' => $matching_product_wrapper->{$field_name}->raw()
        ];
      }
    }
  }


  $price = commerce_currency_format($matching_product_wrapper->commerce_price->amount->value(), 'USD');

  $return = [
    'attributes' => $attributes,
    'price' => $price,
    'sku' => $matching_product->sku,
    'default_product' => $matching_product_wrapper->value(),
    'default_pole_pocket_option' => $matching_product_wrapper->field_pole_pocket_option->value()->name
  ];

  return $return;
}

/**
 * Lists all available options for final banners config form.
 */
function ups_center_gst_banners_all_options() {
  $display = ups_core_load_node_by_title('Banners', 'product_display');

  foreach ($display->field_products['und'] as $prod) {
    $product = commerce_product_load($prod['product_id']);
    $wrap = entity_metadata_wrapper('commerce_product', $product);

    if ($product->status == 1) {
      $runsizes[] = $wrap->field_runsize_number->value()->name;
      $sizes[] = $wrap->field_product_size->value()->name;
      $stocks[] = $wrap->field_stock_option->value()->name;
      $grommets[] = $wrap->field_grommet_option->value()->name;
      $hems[] = $wrap->field_hem_option->value()->name;
      $pole_pockets[] = $wrap->field_pole_pocket_option->value()->name;
    }
  }

  $runsizes = array_unique($runsizes);
  $sizes = array_unique($sizes);
  $stocks = array_unique($stocks);
  $grommets = array_unique($grommets);
  $hems = array_unique($hems);
  $pole_pockets = array_unique($pole_pockets);

  $return = [
    'runsize_number' => array_values($runsizes),
    'product_size' => array_values($sizes),
    'stock_option' => array_values($stocks),
    'grommet_option' => array_values($grommets),
    'hem_option' => array_values($hems),
    'pole_pocket_option' => array_values($pole_pockets)
  ];

  return $return;
}
