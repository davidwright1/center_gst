<?php
/**
 * Created by PhpStorm.
 * User: dwright
 * Date: 2019-05-13
 * Time: 16:03
 */

/**
 * Save Quote to BO.
 */
function ups_center_gst_save_quote($quote) {
  $options = [
    'method' => 'POST',
    'headers' => ['Content-Type' => 'application/json'],
    'data' => drupal_json_encode($quote)
  ];

  $config = ups_center_gst_determine_environment();

  $url = $config['bo_url'] . '/quotes/ups_quote/update_quote';
  $save = drupal_http_request($url, $options);
  watchdog('GST: Quote Saved to BO', drupal_json_encode($save));
  return $save->data;
}

/**
 * create proper CORS Headers
 */
function ups_center_gst_cors_headers() {
  drupal_add_http_header('Access-Control-Allow-Origin', '*');
  //drupal_add_http_header('Access-Control-Allow-Headers','origin, content-type, accept');
}

/**
 * Returns field list that's convenient for fuckery.
 */
function ups_center_gst_field_list() {
  $field_list = [
    'field_runsize_number' => 'tid',
    'field_product_size' => 'tid',
    'field_stock_option' => 'tid',
    'field_coating_option' => 'tid',
    'field_color_option' => 'tid',
    'field_folding_option' => 'tid',
    'field_quote_sku' => 'value',
    'field_quote_price' => 'value',
    'field_quote_usage' => 'value',
    'field_flat_product_collection' => 'value',
    'field_pole_pocket_option' => 'tid',
    'field_hem_option' => 'tid',
    'field_grommet_option' => 'tid',
  ];

  return $field_list;
}

/**
 * Returns list of valid targeted changed fields for GST add-to-cart form.
 */
function _ups_center_gst_target($target) {
  $targets = [
    'runsizes' => 'runsize_number',
    'productSizes' => 'product_size',
    'stockOptions' => 'stock_option',
    'coatingOptions' => 'coating_option',
    'colorOptions' => 'color_option',
    'foldingOptions' => 'folding_option',
    'grommetOptions' => 'grommet_option',
    'hemOptions' => 'hem_option',
    'polePocketOptions' => 'pole_pocket_option',
    'runsize_number' => 'runsize_number'
  ];

  return $targets[$target];
}

/**
 *
 */
function _ups_center_gst_folded_types() {
  $array = [
    'brochures',
    'envelopes',
    'flyers',
    'greeting_cards',
    'indoor_magnets',
  ];
  return $array;
}

/**
 * Returns list of products with matching attributes, given a Quote.
 */
function ups_center_gst_matching_products($data) {
  $quote = is_array($data) ? $data['quote'] : $data->quote;
  $target_data = is_array($data) ? $data['target'] : $data->target;
  $changed = is_array($data) ? $data['changed'] : $data->changed;

  $quote = (object)$quote;
  foreach ($quote->field_quote_type['und'] as $vals) {
    $types[] = $vals['value'];
  }

  // Normalize tids because BO tids != Center tids.
  $field_list = ups_center_gst_field_list();
  foreach ($field_list as $field => $type) {
    if ($type == 'tid') {
      $property = str_replace('field_', '', $field);
      if (!empty($quote->$property) && !is_null($quote->$property)) {
        $terms = taxonomy_get_term_by_name($quote->$property, $property);
        if ($terms) {
          $term = array_shift($terms);
          // Assemble easy to use attribute => tid.
          $attributes[$field] = $term->tid;
        }
      }
    }
  }

  // Identify the changed attribute - limits options to products with that new attribute value.
  if ($changed != 'undefined') {
    $target = _ups_center_gst_target($target_data);
    $target_field = 'field_' . $target;

    if ($target == 'grommet_option') {
      $target = 'product_option_grommets';
    } elseif ($target == 'hem_option') {
      $target = 'product_option_hems';
    } elseif ($target == 'pole_pocket_option') {
      $target = 'product_option_pole_pockets';
    }

    $target_terms = taxonomy_get_term_by_name($changed, $target);
    $target_value = array_shift($target_terms);
    $target_tid = $target_value->tid;
    $attributes = [$target_field => $target_tid] + $attributes;
  }

  $display = node_load($quote->display_id);
  if ($display) {
    $wrapper = entity_metadata_wrapper('node', $display);
    $products = $wrapper->field_products->value();

    // Create a list of all products associated with each attribute for
    // filtering.
    $attributes_products = array_fill_keys(array_keys($attributes), array());
    foreach ($products as $product) {
      if ($product->status == 1) {
        $product_wrapper = entity_metadata_wrapper('commerce_product', $product);

        // Store which attributes this product is associated with.
        foreach ($attributes as $attribute_name => $attribute_value) {
          if ($product_wrapper->{$attribute_name}->raw() == $attribute_value) {
            $attributes_products[$attribute_name][$product->product_id] = $product;
          }
        }
      }
    }

    $filtered_products = [];

    // Filter out products starting with those not associated with the changed
    // attribute and proceeding with those not associated with other attributes
    // in the order the attributes appear in the form.
    $matching_products = array_shift($attributes_products);
    foreach ($attributes_products as $attribute_name => $attribute_products) {
      $filtered_products = array_intersect_key($matching_products, $attribute_products);

      if (!empty($filtered_products)) {
        $matching_products = $filtered_products;
      } else {
        break;
      }
    }
  }

  return $matching_products;
}

/**
 * Determine our environment.
 */
function ups_center_gst_determine_environment() {
  //
  // Determine environment and center id.
  $environment = FALSE;
  $bo_url = '';
  $current_url = url(current_path(), array('absolute' => TRUE));
  $gst_url = 'http://gst.upsstoreprint.com';
  // Center ID
  $center_id = variable_get('ups_center_id', FALSE) ? variable_get('ups_center_id', FALSE) : '';

  if (variable_get('local_development_mode', FALSE)) {
    $environment = 'local';
    $bo_url = 'http://tupssbo.local.test';
    $gst_url = 'http://localhost:4200';
    $center_url = 'tupsscenteredge.local.test';
  } else {
    $envs = [
      'staging' => '-staging-',
      'qa' => '-qa-',
      'prod' => 'upsstoreprint'
    ];

    foreach ($envs as $env => $test_string) {
      if (strstr($current_url, $test_string)) {
        $environment = $env;
      }
    }
    if ($environment == 'staging' || $environment == 'qa') {
      $bo_url = 'https://backoffice-' . $environment . '-tupss.printsites.net';
      $center_url = 'https://' . $center_id . '-' . $environment . '-tupss.printsites.net';
      $gst_url = $gst_url . '/' . $environment;
    } elseif ($environment == 'prod') {
      $bo_url = 'https://backoffice.upsstoreprint.com';
      $center_url = 'https://store' . $center_id . '.upsstoreprint.com';
    }
  }

  $config = [
    'environment' => $environment,
    'center_id' => $center_id,
    'src_url' => $gst_url,
    'bo_url' => $bo_url,
    'center_url' => $center_url
  ];

  return $config;
}

/**
 * Constructs simple form for a button with a callback that either sends user to
 * the cart or creates a line item from a Quote then adds to cart.
 */
function ups_center_gst_quotes_buy_form($form, &$form_state, $vars) {
  $delta = $vars['quote']['qid'];
  $form[$delta]['buy'] = [
    '#type' => 'submit',
    '#value' => 'Buy Now',
    '#submit' => ['_ups_center_gst_buy_quote'],
    '#attributes' => ['class' => ['quote-buy-btn btn-warning']]
  ];
  $form[$delta]['buy_quote'] = [
    '#type' => 'hidden',
    '#value' => $vars['quote']['qid']
  ];
  $form[$delta]['buy_user'] = [
    '#type' => 'hidden',
    '#value' => $vars['user']->uid
  ];

  return $form;
}

/**
 * Constructs simple form for a button with a callback that sends an email.
 */
function ups_center_gst_quotes_email_form($form, &$form_state, $vars) {
  $delta = $vars['quote']['qid'];
  $form[$delta]['mail'] = [
    '#type' => 'submit',
    '#value' => 'Email Quote to User',
    '#submit' => ['_ups_center_gst_email_user'],
    '#attributes' => ['class' => ['quote-email-btn', 'btn-success']]
  ];
  $form[$delta]['mail_quote'] = [
    '#type' => 'hidden',
    '#value' => $vars['quote']['qid']
  ];
  $form[$delta]['mail_user'] = [
    '#type' => 'hidden',
    '#value' => $vars['user']->uid
  ];

  return $form;
}

/**
 * Submit callback for the User to buy a Quote.
 * @param $form
 * @param $form_state
 */
function _ups_center_gst_buy_quote($form, &$form_state) {
  global $user;
  $owner = user_load($form_state['input']['buy_user']);
  $quote = ups_center_gst_get_quote($form_state['input']['buy_quote']);
  $present = FALSE;

  $uid = $owner->uid;
  if (!user_is_logged_in()) {
    $uid = 0;
  }

  if ($user->uid != 0 && $user->mail != $quote['email']) {
    $id = variable_get('ups_center_id', FALSE);
    $center = ($id) ? ups_center_core_get_center($id) : FALSE;
    $path = $center->field_center_calculated_site['und'][0]['value'] . '/user/logout?destination=quote?uuid=' . $quote['uuid'];
    drupal_set_message("Logged in user's email does not match Email on Quote. Please <a href='$path'>log out</a> of center site to add project to cart.");
  }

  ups_center_gst_add_quote_to_cart($uid, $quote);
}

/**
 * Submit callback for the Email Quote to User form.
 * @param $form
 * @param $form_state
 */
function _ups_center_gst_email_user($form, &$form_state) {
  // Email User.
  $user = user_load($form_state['input']['mail_user']);
  $quote = ups_center_gst_get_quote($form_state['input']['mail_quote']);

  $billing = isset($quote['field_quote_billing_profile']['und']) ? commerce_customer_profile_load($quote['field_quote_billing_profile']['und'][0]['profile_id']) : NULL;
  $shipping = isset($quote['field_quote_shipping_profile']['und']) ? commerce_customer_profile_load($quote['field_quote_shipping_profile']['und'][0]['profile_id']) : NULL;
  $order_id = isset($quote['field_quote_order']['und']) ? commerce_order_load($quote['field_quote_order']['und'][0]['target_id']) : NULL;
  $product = commerce_product_load_by_sku($quote['sku']);

  $to = $user->mail;
  $language = $user->language;

  $center = ups_center_core_get_center($quote['center_id']);

  // Holds table of quote basics, user basics center address, and items quoted.
  $params = [
    // center info
    'center_title' => $center->title,
    'center_street_address' => $center->field_center_address['und'][0]['thoroughfare'],
    'center_address' => $center->field_center_address['und'][0]['locality'] .
      ', ' . $center->field_center_address['und'][0]['administrative_area'] . ' '
      . $center->field_center_address['und'][0]['postal_code'],
    'center_phone' => $center->field_center_phone['und'][0]['value'],
    'center_email' => $center->field_center_email['und'][0]['value'],
    'center' => $center,
    'center_url' => $center->field_center_url['und'][0]['value'],

    // quote info
    'qid' => $quote['qid'],
    'date' => date('m/d/Y', $quote['created']),
    'total' => $quote['field_quote_price']['und'][0]['amount'],
    'status' => $quote['status'],
    'uuid' => $quote['uuid'],

    // user info
    'name' => $user->name,
    'order_id' => $order_id,

    // items info
    'quote_title' => $quote['title'],
    'quote_price' => commerce_currency_format($quote['field_quote_price']['und'][0]['amount'], 'USD'),
    'quote_description' => $product->title,
  ];

  // billing info
  if (!is_null($billing)) {
    $bill_params = [
      'billing_street' => $billing->commerce_customer_address['und'][0]['thoroughfare'],
      'billing_address' => $billing->commerce_customer_address['und'][0]['locality'] .
        ', ' . $billing->commerce_customer_address['und'][0]['administrative_area'] . ' '
        . $billing->commerce_customer_address['und'][0]['postal_code'],
      'billing_phone' => $billing->field_phone['und'][0]['value'],
      'bill_to' => $quote['field_quote_billing_profile']['und'][0]['profile_id'],
    ];
    $params += $bill_params;
  }

  // shipping info
  if (!is_null($shipping)) {
    $ship_params = [
      'shipping_street' => $shipping->commerce_customer_address['und'][0]['thoroughfare'],
      'shipping_address' => $shipping->commerce_customer_address['und'][0]['locality'] .
        ', ' . $shipping->commerce_customer_address['und'][0]['administrative_area'] . ' '
        . $shipping->commerce_customer_address['und'][0]['postal_code'],
      'ship_to' => $quote['field_quote_shipping_profile']['und'][0]['profile_id'],
    ];

    $params += $ship_params;
  }


  $center_url = (variable_get('local_development_mode', FALSE)) ? $params['center_url'] : 'https://' . $params['center_url'];
  $params['quote_url'] = $center_url . '/quote?uuid=' . $params['uuid'];

  drupal_mail('ups_order_notification', 'quote_email', $to, $language, $params);
  drupal_set_message('Emailed quote information to ' . $to . '.');
}

/**
 * Generates a line item in our two workflows.
 */
function _ups_center_gst_create_line_item($project, $type, $order) {

  switch ($type) {
    case 'printsites_project':
      $product = commerce_product_load($project->field_project_product['und'][0]['target_id']);

      $line_item = commerce_line_item_new('printsites_project', $order->order_id);
      $line_item->line_item_label = $project->title;
      $line_item->quantity = taxonomy_term_load($product->field_runsize_number['und'][0]['tid'])->name;
      $line_item->data = [];
      $line_item->commerce_product = ['und' => [0 => ['product_id' => $product->product_id]]];
      $line_item->field_print_project = ['und' => [0 => ['target_id' => $project->id]]];
      $line_item->commerce_total = [
        'und' => [
          0 => [
            'amount' => $product->commerce_price['und'][0]['amount'],
            'currency_code' => 'USD',
            'data' => [
              'components' => [
                0 => [
                  'name' => 'base_price',
                  'included' => TRUE,
                  'price' => [
                    'amount' => '',
                    'currency_code' => 'USD'
                  ]
                ]
              ]
            ]
          ]
        ]
      ];
      $line_item->commerce_unit_price = [
        'und' => [
          0 => [
            'amount' => ($product->commerce_price['und'][0]['amount'] / 100),
            'currency_code' => 'USD',
            'data' => [
              'components' => [
                0 => [
                  'name' => 'base_price',
                  'included' => TRUE,
                  'price' => [
                    'amount' => ($product->commerce_price['und'][0]['amount'] / 100),
                    'currency_code' => 'USD'
                  ]
                ]
              ]
            ]
          ]
        ]
      ];
      commerce_line_item_save($line_item);
      return $line_item;


      break;

    case 'job':
      $product = commerce_product_load_by_sku($project->product->uuid);
      $line_item = commerce_line_item_new('product', $order->order_id);

      $title = (isset($project->project->title)) ? $project->project->title : '';
      $title = ($title == '') ? $project->project->name : $title;

      $line_item->line_item_label = $title;
      $line_item->quantity = '1.00';
      $line_item->data = [];
      $line_item->commerce_product = ['und' => [0 => ['product_id' => $product->product_id]]];
      $line_item->field_ps_core_job_id = ['und' => [0 => ['value' => $project->job_uuid, 'format' => NULL, 'safe_value' => $project->job_uuid]]];
      $line_item->commerce_total = [
        'und' => [
          0 => [
            'amount' => $product->commerce_price['und'][0]['amount'],
            'currency_code' => 'USD',
            'data' => [
              'components' => [
                0 => [
                  'name' => 'base_price',
                  'included' => TRUE,
                  'price' => [
                    'amount' => '',
                    'currency_code' => 'USD'
                  ]
                ]
              ]
            ]
          ]
        ]
      ];
      $line_item->commerce_unit_price = [
        'und' => [
          0 => [
            'amount' => ($product->commerce_price['und'][0]['amount'] / 100),
            'currency_code' => 'USD',
            'data' => [
              'components' => [
                0 => [
                  'name' => 'base_price',
                  'included' => TRUE,
                  'price' => [
                    'amount' => ($product->commerce_price['und'][0]['amount'] / 100),
                    'currency_code' => 'USD'
                  ]
                ]
              ]
            ]
          ]
        ]
      ];
      commerce_line_item_save($line_item);
      return $line_item;
      break;
  }
}

/**
 * Adds line item to cart.
 *
 * @param $uid
 * @param $quote
 */
function ups_center_gst_add_quote_to_cart($uid, $quote) {
  $present = FALSE;
  $cart = commerce_cart_order_load($uid);
  if ($cart && $cart->commerce_line_items != []) {
    // Determine if line item is a Quote.
    $order_wrapper = entity_metadata_wrapper('commerce_order', $cart);

    foreach ($order_wrapper->commerce_line_items->value() as $line_item) {
      $project = FALSE;
      if ($line_item->type == 'product' || $line_item->type == 'printsites_project') {
        if (isset($line_item->field_print_project)) {
          $project = printsites_project_load($line_item->field_print_project['und'][0]['target_id']);
        } elseif (isset($line_item->field_ps_core_job_id)) {
          $project = ps_core_get_job($line_item->field_ps_core_job_id['und'][0]['value']);
        }
      }
      //
      // Continue if Quote project line item is already in cart.
      if ($project) {
        // printsites_project line item workflow
        if (isset($project->field_quote_id)) {
          if ($project->field_quote_id['und'][0]['value'] == $quote['qid']) {
            $present = TRUE;
          }
        } // product line item workflow
        elseif (isset($project->meta)) {
          $data = drupal_json_decode($project->meta);
          if ($data->qid == $quote['qid']) {
            $present = TRUE;
          }
        }
      }
    }
    if ($present) {
      drupal_goto('/cart');
    }

  }
  // Else there is no cart, there are no line items in cart, or no cart line items match.
  if ($cart->commerce_line_items == [] || $present == FALSE || is_null($cart)) {
    $line_item = FALSE;
    // Try to load projects/jobs.
    if (!empty($quote['field_quote_project'])) {
      $project = printsites_project_load($quote['field_quote_project']['und'][0]['target_id']);
    }
    elseif (!empty($quote['project_pscore_id'])) {
      $projects = ps_core_get_job($quote['project_pscore_id']);
    }
    // Having project/job, create new line item for an order.
    // Use existing cart or create new order.
    if ($cart) {
      $order = $cart;
    } else {
      $order = commerce_order_new($uid, 'cart');
      commerce_order_save($order);
    }
    if ($projects) {
      $line_item = _ups_center_gst_create_line_item($projects, 'job', $order);
    } elseif (is_object($project)) {
      $line_item = _ups_center_gst_create_line_item($project, 'printsites_project', $order);
    }

    if ($line_item) {
      $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
      $order_wrapper->commerce_line_items[] = $line_item;
      $order_wrapper->save();
    }
    commerce_cart_order_session_save($order->order_id);
  }

  drupal_goto('cart');
}
