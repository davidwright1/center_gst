<?php
/**
 * @file
 * Theme functions for Drupal-side GST.
 */

/**
 * Implements hook_preprocess_HOOK().
 */
function ups_center_gst_preprocess_quote_landing(&$vars) {

  if ($vars['mode'] == 'anonymous') {
    $vars['title'] = 'Review your Quote';
  }
  else {
    $vars['title'] = "You've finished creating your Quote!";
  }

  $quote = $vars['quote'];
  if (is_string($vars['quote'])) {
    $quote = drupal_json_decode($quote);
  }

  $config = ups_center_gst_determine_environment();
  $bo_url = str_replace(['http://', 'https://'], '', $config['bo_url']);
  $center_url = str_replace(['http://', 'https://'], '', $config['center_url']);

  $vars['gst_url'] = $config['src_url'] . '?bo_url=' . $bo_url . '&center_url=' . $center_url;

  $vars['quote_title'] = $quote['title'];
  $vars['quote_price'] = commerce_currency_format($quote['field_quote_price']['und'][0]['amount'], 'USD');

  // Template based project.
  if (!empty($quote['field_quote_project'])) {
    $project = printsites_project_load($quote['field_quote_project']['und'][0]['target_id']);
    if (!empty($project->field_preview_url)) {
      $img_preview = $project->field_preview_url['und'][0]['url'];
    }
  }
  // Upload based project.
  elseif (!empty($quote['project_pscore_id']) && $quote['field_quote_project'] == []) {
    $pscore_job = ps_core_get_job($quote['project_pscore_id']);
    $img_preview = $pscore_job->project->production_asset->preview->uri;
  }

  if (!empty($quote['sku'])) {
    $product = commerce_product_load_by_sku($quote['sku']);
  }

  // TODO: Some of my products save skus in different places?
  $vars['img_preview'] = $img_preview;
  if ($product) {
    $vars['product_title'] = $product->title;
  }


  $user = user_load_by_mail($quote['email']);
  if ($user) {
    $vars['user'] = $user;
  }

  $form_parameters = [
    'user' => $vars['user'],
    'quote' => $quote
  ];
  $email_form = drupal_get_form('ups_center_gst_quotes_email_form', $form_parameters);
  $output = drupal_render($email_form);
  $vars['quote_email_button'] = $output;

  $buy_form = drupal_get_form('ups_center_gst_quotes_buy_form', $form_parameters);
  $buy_output = drupal_render($buy_form);
  $vars['buy_now_button'] = $buy_output;

  drupal_set_title('You\'ve created your ' . $quote['title']);
}
