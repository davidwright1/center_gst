<?php
/**
 * @file
 * ups_center_gst.services.inc
 */

/**
 * Implements hook_default_services_endpoint().
 */
function ups_center_gst_default_services_endpoint() {
  $export = [];

  $endpoint = new stdClass();
  $endpoint->disabled = FALSE; /* Edit this to true to make a default endpoint disabled initially */
  $endpoint->api_version = 3;
  $endpoint->name = 'gst';
  $endpoint->server = 'rest_server';
  $endpoint->path = 'gst';
  $endpoint->authentication = array(
    'services_accept_origin' => array(
      'whitelist' => '*',
      'no_origin_policy' => 0,
    ),
  );
  $endpoint->server_settings = array(
    'formatters' => array(
      'json' => TRUE,
      'bencode' => FALSE,
      'jsonp' => FALSE,
      'php' => FALSE,
      'xml' => FALSE,
    ),
    'parsers' => array(
      'application/json' => TRUE,
      'application/x-www-form-urlencoded' => FALSE,
      'application/xml' => FALSE,
      'multipart/form-data' => FALSE,
      'text/xml' => FALSE,
    ),
  );
  $endpoint->resources = array(
    'gst' => array(
      'actions' => array(
        'start_quote' => array(
          'enabled' => '1',
        ),
        'quote_sizes' => array(
          'enabled' => '1',
        ),
        'refresh_attributes' => array(
          'enabled' => '1',
        ),
        'validate' => array(
          'enabled' => '1',
        ),
        'templates' => array(
          'enabled' => '1',
        ),
        'upload' => array(
          'enabled' => '1',
        ),
        'banners_options' => array(
          'enabled' => '1',
        ),
        'banners_skus' => array(
          'enabled' => '1',
        ),
        'banners_attributes' => array(
          'enabled' => '1',
        ),
        'banners_all_options' => array(
          'enabled' => '1',
        ),
      ),
    ),
  );
  $endpoint->debug = 0;

  $export['gst'] = $endpoint;

  return $export;
}
